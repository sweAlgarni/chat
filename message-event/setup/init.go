package setup

import (
	"github.com/elgarni/Go-Kafka/message-event/auth"
	"github.com/elgarni/Go-Kafka/message-event/converter"
	"github.com/elgarni/Go-Kafka/message-event/service"
	"github.com/iris-contrib/middleware/cors"
	"github.com/kataras/iris"
	"log"
)

func Init() *iris.Application {
	app := iris.Default()
	crs := cors.New(cors.Options{
		AllowedOrigins:   []string{"*"}, // allows everything, use that to change the hosts.
		AllowedMethods:    []string{"*"},
		AllowedHeaders:     []string{"*"},
		AllowCredentials: true,

	})
	app.Use(crs)

	// register routes
	app.Post("/messages/newMessage", createNewMessageEvent)
	app.Get("/messages/status", serviceStatus)
	app.Post("/messages/authenticate", authenticateUser)

	return app
}
func serveHome(ctx iris.Context) {
	ctx.ServeFile("home.html" , true)
}
func authenticateUser(ctx iris.Context) {
	var accountDto converter.AccountDto
	if err := converter.Serialize(ctx, &accountDto) ; err != nil {
		log.Println(err)
		return
	}

	token := &converter.JwtToken{auth.CreateToken(accountDto.UserName)}
	ctx.JSON(token)
}

func serviceStatus(ctx iris.Context) {
	ctx.JSON("Working...")
}

func createNewMessageEvent(ctx iris.Context) {
	ctx.ResponseWriter().Header().Set("Access-Control-Allow-Origin", "*")
	claims, err := auth.Authenticate(ctx.Request())
	if err != nil {
		ctx.JSON(converter.ErrorDto{Message: "unathorized"})
		return;
	}
	var messageDto converter.MessageDto

	var msg interface{}
	if err := converter.Serialize(ctx, &messageDto); err != nil {
		msg = converter.ErrorDto{"Invalid Input"}
	} else {
		if err := service.CreateNewMessage(messageDto, claims.UserUid); err != nil {
			msg = ApiMessage{"Success"}
		} else {
			msg = ApiMessage{"Error processing request"}
		}
	}

	ctx.JSON(msg)
}

type ApiMessage struct {
	Message string
}
