package converter

import (
	"encoding/json"
	"github.com/kataras/iris"
)

func Serialize(ctx iris.Context, dto interface{}) error {
	dec := json.NewDecoder(ctx.Request().Body)
	err := dec.Decode(&dto)
	return err
}

type ErrorDto struct {
	Message string
}

type MessageDto struct {
	Channel  string
	Text     string
}

type AccountDto struct {
	UserName string `json:"userName"`
}

type JwtToken struct {
	Token string `json:"token"`
}
