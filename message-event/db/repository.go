package db

import (
	"github.com/jinzhu/gorm"
	"time"
)

var dbInstance *gorm.DB


type Message struct {
	ID        int64
	Uid       string
	User      string `gorm:"INDEX"`
	Channel   string `gorm:"INDEX"`
	Text      string
	Timestamp time.Time
}

type Repository struct {
}

func (repo Repository) Save(msg *Message) {
	if db := dbInstance.Create(&msg) ; db.Error != nil {
		log.Print(db.Error)
	} else {
		log.Print("Message saved successfully")
	}
}





