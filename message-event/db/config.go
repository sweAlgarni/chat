package db

import (
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"os"
	"time"
)

type Properties struct {
	Host     string
	Port     string
	Name     string
	User     string
	Password string
}

type logger struct {
}

var properties = Properties{
	Host: os.Getenv("DB_HOST"),
	Port: os.Getenv("DB_PORT"),
	Name: os.Getenv("DB_NAME"),
	Password: os.Getenv("DB_PASSWORD"),
	User: os.Getenv("DB_USER"),
}

var log = logger{}

var maxConnectionTries = 100

func init() {
	postgresConnectionLink := fmt.Sprintf("host=%s port=%s user=%s dbname=%s password=%s sslmode=disable",
		properties.Host, properties.Port, properties.User, properties.Name, properties.Password)
	fmt.Println(postgresConnectionLink)

	for i := 1 ; i <= maxConnectionTries ; i++{
		db, err := gorm.Open("postgres", postgresConnectionLink)
		if err != nil {
			log.Print(fmt.Sprintf("Error connecting to the database : %s\n Trying again, attempt #%d\n" , err, i))
			time.Sleep(2 * time.Second)
		} else {
			dbInstance = db
			break
		}
	}
	if dbInstance == nil {
		panic(fmt.Sprintf("Could not connect to database after %d tries\n", maxConnectionTries))
	}

	dbInstance.LogMode(true)
	dbInstance.SetLogger(log)
	dbInstance.AutoMigrate(&Message{})
}

func (logger logger) Print(values...interface{})  {
	fmt.Printf("[DB] %s %q\n", time.Now().Format("2006-01-02 15:04:05"), values)
}