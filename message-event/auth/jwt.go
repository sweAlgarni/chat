package auth

import (
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/segmentio/ksuid"
	"log"
	"net/http"
	"strings"
	"time"
)

var secret = []byte("123456") // os.Getenv("")

const (
	XAuthorization = "X-Authorization"
	Cookie	 = "Cookie"
	Bearer        = "Bearer"
	Space		  = " "
)

type ChatClaim struct {
	UserUid string
	jwt.StandardClaims
}

func Authenticate(req *http.Request) (*ChatClaim, error) {
	token,err := req.Cookie(XAuthorization)

	if err != nil {
		return  nil , err
	}

	return parseToken(strings.TrimSpace(token.Value))
}

func parseToken(token string) (*ChatClaim, error) {
	parsedToken, err := jwt.ParseWithClaims(token, &ChatClaim{}, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}
		return secret, nil
	})
	if err != nil {
		log.Println(err)
		return nil, err
	}

	if claims, ok := parsedToken.Claims.(*ChatClaim); ok && parsedToken.Valid {
		return claims, nil
	}

	return nil, fmt.Errorf("error parsing the claims")
}

func CreateToken(userName string) string {
	// Create a new token object, specifying signing method and the claims
	// you would like it to contain.

	token := jwt.NewWithClaims(jwt.SigningMethodHS256,ChatClaim{
		UserUid: ksuid.New().String(),
		StandardClaims: jwt.StandardClaims{
			Audience: "dd",
			ExpiresAt: time.Now().Add(1000 * time.Minute).Unix(),
		},
	})

	// Sign and get the complete encoded token as a string using the secret
	tokenString, _ := token.SignedString(secret)
	return tokenString
}
