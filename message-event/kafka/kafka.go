package kafka

import (
	"encoding/json"
	"fmt"
	"github.com/Shopify/sarama"
	"github.com/elgarni/Go-Kafka/message-event/db"
)

type EventType string

type EventMessage struct {
	EventType EventType
	Data      db.Message
}

const (
	NewMessage EventType = "NewMessage"
)

var syncProducer sarama.SyncProducer


func SendEvent(eventMessage EventMessage) error{
	eventAsJson,encodingError := json.Marshal(eventMessage)

	if encodingError != nil{
		return encodingError
	}
	eventAsString := string(eventAsJson)
	fmt.Println("event: " + eventAsString)


	msg := &sarama.ProducerMessage{
		Topic: properties.Topic,
		Value: sarama.StringEncoder(fmt.Sprint(eventAsString)),
	}
	_, _, sendingError := syncProducer.SendMessage(msg)
	if sendingError != nil {
		return sendingError
	}

	return nil
}

