package kafka

import (
	"fmt"
	"github.com/Shopify/sarama"
	"log"
	"os"
)

type Properties struct {
	BrokerAddress string
	Topic         string
}

var properties = Properties{
	Topic: os.Getenv("KAFKA_TOPIC"),
	BrokerAddress: os.Getenv("KAFKA_BROKER_ADDRESS"),
}
func init() {

	// syncProducer config
	config := sarama.NewConfig()
	config.Producer.Return.Successes = true
	config.Producer.Return.Errors = true
	config.Producer.RequiredAcks = sarama.WaitForAll
	config.Producer.Retry.Max = 5

	//verbose debugging (comment this line to disabled verbose sarama logging)
	sarama.Logger = log.New(os.Stdout, "[sarama] ", log.LstdFlags)

	// remote details
	brokerAddress := []string{properties.BrokerAddress}

	producer, err := sarama.NewSyncProducer(brokerAddress, config)
	if err != nil {
		panic(fmt.Sprintf("Error initializing Kafka : %s \n%s", err,properties))
	}

	syncProducer = producer

}

func (prop Properties) String() string {
	return properties.Topic + "|" + prop.BrokerAddress
}