package service

import (
	"github.com/elgarni/Go-Kafka/message-event/converter"
	"github.com/elgarni/Go-Kafka/message-event/db"
	"github.com/elgarni/Go-Kafka/message-event/kafka"
	"github.com/segmentio/ksuid"
	"time"
)

func CreateNewMessage(messageDto converter.MessageDto, userUid string) error {

	dbMessage := db.Message{
		Channel:   messageDto.Channel,
		User:      userUid,
		Text:      messageDto.Text,
		Timestamp: time.Now(),
		Uid:       ksuid.New().String(),
	}

	db.Repository{}.Save(&dbMessage)

	eventMessage := kafka.EventMessage{
		EventType: kafka.NewMessage,
		Data: dbMessage,
	}

	err := kafka.SendEvent(eventMessage)

	return err
}
