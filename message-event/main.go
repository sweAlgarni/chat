package main

import (
	_ "github.com/elgarni/Go-Kafka/message-event/db"
	_ "github.com/elgarni/Go-Kafka/message-event/kafka"
	"github.com/elgarni/Go-Kafka/message-event/setup"

	"github.com/kataras/iris"
)

func main(){


	app := setup.Init()
	app.Logger().SetLevel("info")
	app.Run(iris.Addr(":3000"))
}
