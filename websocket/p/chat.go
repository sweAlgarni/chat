package p

import (
	"fmt"
	"log"
	"time"
)

type ChatServer struct {
	Rooms          map[string][]string
	Users          map[string]*User
	JoinRoom       chan map[string]string
	RegisterUser   chan *User
	UnRegisterUser chan *User
	Broadcast      chan ChatMessage
}

type ChatMessage struct {
	Uid       string `json:"uid"`
	User      string `json:"user"`
	Channel   string `json:"channel"`
	Text      string `json:"text"`
	Timestamp time.Time `json:"timestamp"`
}

func (chat *ChatServer) Run() {
	for {
		select {
		case joinMap := <-chat.JoinRoom:
			roomName := joinMap["roomName"]
			userId := joinMap["userId"]
			_, exist := chat.Rooms[roomName]
			if exist {
				chat.Rooms[roomName] = append(chat.Rooms[roomName], userId)
				log.Println(fmt.Sprintf("user {%s} added to room {%s}", userId, roomName))
			} else {
				chat.Rooms[roomName] = []string{userId}
				log.Println(fmt.Sprintf("Creating room..\nuser {%s} added to room {%s}", userId, roomName))
			}
		case user := <-chat.RegisterUser:
			chat.Users[user.uid] = user
			log.Println(fmt.Sprintf("Storing user in index {%s}", user.uid))

		case user := <-chat.UnRegisterUser:
			delete(chat.Users, user.uid)
			log.Println(fmt.Sprintf("Removed user {%s} from undex", user.uid))

		case chatMessage := <-chat.Broadcast:
			log.Println(fmt.Sprintf("broadcasting message with id {%s} to room {%s}", chatMessage.Uid , chatMessage.Channel))
			go func() {

				for _,roomUserId := range chat.Rooms[chatMessage.Channel] {
					user, exist := chat.Users[roomUserId]
					if exist {
						user.send <- chatMessage
					} else {
						// handle user removal after leaving
					}
					log.Println(fmt.Sprintf("Sending msg with id {%s} to user with id {%s}", chatMessage.Uid, roomUserId))
				}
			}()
		}
	}
}

func NewChat() *ChatServer {
	return &ChatServer{
		Rooms:          make(map[string][]string),
		Users:          make(map[string]*User),
		JoinRoom:       make(chan map[string]string),
		RegisterUser:   make(chan *User),
		UnRegisterUser: make(chan *User),
		Broadcast:      make(chan ChatMessage),

	}
}
