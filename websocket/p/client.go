// Copyright 2013 The Gorilla WebSocket Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package p

import (
	"encoding/json"
	"github.com/elgarni/Go-Kafka/websocket/auth"
	"log"
	"net/http"
	"time"

	"github.com/gorilla/websocket"
)

const (
	// Time allowed to write a message to the peer.
	writeWait = 10 * time.Second

	// Time allowed to read the next pong message from the peer.
	pongWait = 4 * time.Second

	// Send pings to peer with this period. Must be less than pongWait.
	pingPeriod = (pongWait * 9) / 10

	// Maximum message size allowed from peer.
	maxMessageSize = 512
)

var (
	newline = []byte{'\n'}
	space   = []byte{' '}
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

// User is a middleman between the websocket connection and the room.
type User struct {

	uid string

	// The websocket connection.
	conn *websocket.Conn

	// Buffered channel of outbound messages.
	send chan ChatMessage
}

var hashtable = make(map[string]*User)



// WritePump pumps messages from the room to the websocket connection.
//
// A goroutine running WritePump is started for each connection. The
// application ensures that there is at most one writer to a connection by
// executing all writes from this goroutine.
func (user *User) WritePump(chat *ChatServer) {
	ticker := time.NewTicker(pingPeriod)
	defer func() {
		ticker.Stop()
		user.conn.Close()
	}()
	for {
		select {
		case message, ok := <-user.send:
			log.Println("New message in channel")
			jsonMessage, err := json.Marshal(message)
			user.conn.SetWriteDeadline(time.Now().Add(writeWait))
			if !ok {
				// The room closed the channel.
				//user.conn.WriteMessage(websocket.CloseMessage, []byte{})
				log.Println("Not ok")
				return
			}

			w, err := user.conn.NextWriter(websocket.TextMessage)
			if err != nil {
				log.Println(err)
				return
			}
			log.Println("Writing msg to weiter")
			w.Write(jsonMessage)

			//Add queued chat messages to the current websocket message.
			//n := len(user.send)
			//log.Println(fmt.Sprintf("length of channel is %d\n" , n))
			//for i := 0; i < n; i++ {
			//	w.Write([]byte("Some space between buffered messages\n"))
			//	w.Write(<-user.send)
			//}

			if err := w.Close(); err != nil {
				log.Println(err)
				return
			}
		case <-ticker.C:
			user.conn.SetWriteDeadline(time.Now().Add(writeWait))
			if err := user.conn.WriteMessage(websocket.PingMessage, nil); err != nil {
				log.Println("Ticker........")
				log.Println(err)
				chat.UnRegisterUser <- user
				return
			}
		}
	}
}

// ServeWs handles websocket requests from the peer.
func ServeWs(chat *ChatServer, w http.ResponseWriter, r *http.Request) {
	log.Println("Upgrading connection")

	claims, err := auth.Authenticate(r)
	if err != nil {
		log.Println(err)
		return
	}

	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println(err)
		return
	}


	user := &User{ conn: conn,uid:claims.UserUid ,  send: make(chan ChatMessage, 512)}
	chat.RegisterUser <- user

	go user.WritePump(chat)
}
