package kafka

import (
	"encoding/json"
	"fmt"
	"github.com/Shopify/sarama"
	"github.com/elgarni/Go-Kafka/websocket/p"
	"log"
	"os"
	"time"
)

var consumerMessage chan *sarama.ConsumerMessage
type EventType string

type EventMessage struct {
	EventType EventType
	Data      p.ChatMessage
}

func ListenToKafkaMessages(chat *p.ChatServer) {
	master := configureConsumer()
	partitions,_ := master.Partitions(properties.Topic) ;
	for _,partitionId := range partitions {
		consumer, err := master.ConsumePartition(properties.Topic, partitionId, sarama.OffsetNewest)
		if err != nil {
			log.Println(err)
			continue
		}
		go func() {
			for {
				select {
				case consumerError := <-consumer.Errors():
					log.Println(consumerError)
				case msg := <-consumer.Messages():
					log.Println("new message")
					log.Println(fmt.Sprintln("Received messages", string(msg.Value)))
					var eventMessage *EventMessage
					err := json.Unmarshal(msg.Value ,&eventMessage)
					if err != nil {
						log.Println(err)
					} else {
						log.Println("Unmarshal is okay")
					}
					chat.Broadcast <- eventMessage.Data
					log.Println("Send broad")
				}
			}
		}()
	}
}

func configureConsumer() sarama.Consumer {
	// syncProducer config
	config := sarama.NewConfig()
	config.Consumer.Offsets.Initial = sarama.OffsetNewest
	config.Consumer.Offsets.CommitInterval = 1 * time.Second
	config.Consumer.Return.Errors = true


	//verbose debugging (comment this line to disabled verbose sarama logging)
	sarama.Logger = log.New(os.Stdout, "[sarama] ", log.LstdFlags)

	// Create new consumer
	brokers := []string{properties.BrokerAddress}
	master, err := sarama.NewConsumer(brokers, config)
	if err != nil {
		panic(err)
	}
	return master
}