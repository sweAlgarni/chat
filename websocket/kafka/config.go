package kafka

import (
	//"github.com/Shopify/sarama"
	//"log"
	"os"
)

type Properties struct {
	BrokerAddress string
	Topic         string
}

var properties = Properties{
	Topic: os.Getenv("KAFKA_TOPIC"),
	BrokerAddress: os.Getenv("KAFKA_BROKER_ADDRESS"),
}
//func configure() {
//
//	consumeTopicPartitions(master)
//
//}
//
//func consumeTopicPartitions(master sarama.Consumer) {
//	consumerMessage = make(chan *sarama.ConsumerMessage)
//	partitions, err := master.Partitions(properties.Topic)
//	if err != nil {
//		log.Println(err)
//		return
//	}
//	for _,partitionId := range partitions {
//		consumer, err := master.ConsumePartition(properties.Topic, partitionId, sarama.OffsetNewest)
//		if err != nil {
//			log.Println(err)
//			continue
//		}
//		go func() {
//			for {
//				select {
//				case consumerError := <-consumer.Errors():
//					log.Println("err..")
//					log.Println(consumerError)
//				case msg := <-consumer.Messages():
//					log.Println("new message")
//					consumerMessage <- msg
//				}
//			}
//		}()
//	}
//
//}
//
//func (prop Properties) String() string {
//	return properties.Topic + "|" + prop.BrokerAddress
//}