package auth

import (
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"log"
	"net/http"
	"strings"
)

var secret = []byte("123456") // os.Getenv("")

const (
	XAuthorization = "X-Authorization"
	Cookie	 = "Cookie"
	Bearer        = "Bearer"
	Space		  = " "
)

type ChatClaim struct {
	UserUid string
	jwt.StandardClaims
}

func Authenticate(req *http.Request) (*ChatClaim, error) {
	token,err := req.Cookie(XAuthorization)

	if err != nil {
		return  nil , err
	}

	return parseToken(strings.TrimSpace(token.Value))
}

func parseToken(token string) (*ChatClaim, error) {
	parsedToken, err := jwt.ParseWithClaims(token, &ChatClaim{}, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}
		return secret, nil
	})
	if err != nil {
		log.Println(err)
		return nil, err
	}

	if claims, ok := parsedToken.Claims.(*ChatClaim); ok && parsedToken.Valid {
		return claims, nil
	}

	return nil, fmt.Errorf("error parsing the claims")
}
