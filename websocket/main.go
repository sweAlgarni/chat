package main

import (
	"encoding/json"
	"flag"
	"github.com/dgrijalva/jwt-go"
	"github.com/elgarni/Go-Kafka/websocket/auth"
	"github.com/elgarni/Go-Kafka/websocket/kafka"
	"github.com/elgarni/Go-Kafka/websocket/p"
	"log"
	"net/http"
	"time"
)

var addr = flag.String("addr", ":8000", "http service address")
var secret = []byte("123456")

func serveHome(w http.ResponseWriter, r *http.Request) {
	log.Println(r.URL)
	log.Println("Reached here.................................")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	http.ServeFile(w, r, "home.html")
}

func main() {
	flag.Parse()
	chat := p.NewChat()
	printToken()
	go chat.Run()
	go kafka.ListenToKafkaMessages(chat)
	http.HandleFunc("/", serveHome)
	http.HandleFunc("/websocket/ws", func(w http.ResponseWriter, r *http.Request) {
		log.Println("hergjnd============================")
		p.ServeWs(chat , w, r)
	})
	http.HandleFunc("/websocket/subscribe", func(writer http.ResponseWriter, request *http.Request) {
		enterRoom(chat, writer,request)
	})

	http.HandleFunc("/websocket/status", func(writer http.ResponseWriter, request *http.Request) {
		writer.Write([]byte("Websocketing runnnig..."))
	})
	err := http.ListenAndServe(*addr, nil)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}
func printToken() {
	// Create a new token object, specifying signing method and the claims
	// you would like it to contain.
	token := jwt.NewWithClaims(jwt.SigningMethodHS256,auth.ChatClaim{
		UserUid: "4d5djdke83dgsm27j",
		StandardClaims: jwt.StandardClaims{
			Audience: "dd",
			ExpiresAt: time.Now().Add(1000 * time.Minute).Unix(),
		},
	})

	// Sign and get the complete encoded token as a string using the secret
	tokenString, _ := token.SignedString(secret)

	log.Println(tokenString)
}
func enterRoom(chat *p.ChatServer, writer http.ResponseWriter, request *http.Request) {
	claims, err := auth.Authenticate(request);
	if err != nil {
		log.Println(err)
		return
	}

	decoder := json.NewDecoder(request.Body)
	type JsonBody struct {
		RoomName string
	}
	var dto JsonBody
	err = decoder.Decode(&dto)
	if err != nil {
		log.Println(err)
		return
	}

	joinMap := map[string]string{"userId":claims.UserUid , "roomName" : dto.RoomName}
	chat.JoinRoom <- joinMap

}